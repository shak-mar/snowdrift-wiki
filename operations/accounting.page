---
title: Accounting and Budgets
categories: treasury
...

Until Snowdrift.coop is fully operating and has more robust accounting report systems, we're just sharing here a summary of our startup finances. Figures are rounded to nearest dollar. Various expenses personally covered by volunteers are not included here.

## 2019

*last updated 2019-04-29*

### Income

* $200 misc donations

### Expenses

* $146 web hosting
* $170 conference expenses

## 2018

### Income

* $99 misc donations (after fees)

### Expenses

* $600 web hosting
* $20 misc technology services
* $20 legal filing
* $42 conference expenses
* $1,400 past balance on web development contract with Bryan Richter

TOTAL 2018 Expenses: $2,369

## 2017

### Income

* $1,458 misc donations (after fees)

### Expenses

* $559 web hosting and registration

## 2016

### Income

* $27,983 grant from Open Source Initiative (supported by donors giving expressly to help Snowdrift.coop)
* $1,623 misc donations (minus fees)

TOTAL 2016 Income: $29,606

### Expenses

* $28,500 contract work by lead developer Bryan Richter
* $590 banners, stickers, business cards, other misc expenses for conferences
* $275 rewards for donors and promotion etc
* $219 travel costs for conferences
* $558 web hosting, domain costs
* $25 annual report to State of Michigan and address change

TOTAL 2016 Expenses: $30,167

## 2015

### Income

* $8,797 via Tilt Open from our end-of-2014 [launch campaign](https://snowdrift.tilt.com)
* $9,000 donations from various major sponsors outside Tilt campaign
* $14,300 donated by co-founder David Thomas
* $6,840 grant from Open Source Initiative
* $1,634 misc donations (minus fees)

TOTAL 2015 Income: $40,571

### Expenses

* Contract web development work
    * $13,674 to Nikita Karetnikov (includes some currency and transfer fees)
    * $25,200 to Bryan Richter, now lead developer
* $3,000 to [Attorney Deb Olsen](http://www.esoplaw.com/) for legal retainer, initial work on incorporation / bylaws
* $388 banners, stickers, business cards, other misc expenses for conferences
* $1,402 travel costs for conferences
* $180 web hosting, domain costs
* $20 annual report to State of Michigan

TOTAL 2015 Expenses: $43,864

## 2014

### Income

* $2,000 donated by Kate Rosenbloom Pohl
* $7,800 donated by co-founder David Thomas
* $1,442 donated by co-founder Aaron Wolf
* $135 donated by other community supporters prior to launch campaign (after fees)
* [launch campaign](https://snowdrift.tilt.com) income received in 2014 after fees:
    * $686 via Paypal
    * $5,150 via Dwolla
    * 0.34826 BTC donated

TOTAL 2014 income: $16,913 and 0.34826 BTC

### Expenses

* Contract web development:
    * $2,700 to Joseph Thomas
    * $4,320 to Mitchell Rosen
    * $4,321 to Nikita Karetnikov (includes some currency and transfer fees)
* $31 in office supplies
* $138 plane ticket to BayHac conference
* $105 to [Gandi.net](http://gandi.net) for SSL certificate and domain registration
* $20 to State of Michigan for filing Annual Report

TOTAL 2014 expenses: $11,635

## 2013 Finances

After initial discussions in 2012, the first public Snowdrift.coop stuff started in 2013, prior to any formal legal status. Co-founders Aaron Wolf and David Thomas personally covered all costs ($9,000+) for web hosting, domain registration, legal filings, and work from two independent contractors: Kim Hoff from June through November helping with plans, research, legal structure, writings, and presentation; and Joseph Thomas for web development work in December.

## Accounts

At the beginning of 2014, with initial Articles (see [Legal](/legal)) accepted by the State of Michigan and an IRS EIN, we opened a non-interest-bearing checking account with the [National Cooperative Bank](http://ncb.coop).

Separate from the official account, Aaron and David have continued (through 2016) to personally cover expenses including their own cost-of-living while working on Snowdrift.coop, travel costs for various conferences, and some of the server and incidental costs.

## Taxes

All of the income for Snowdrift.coop and all transactions formally part of the co-op's accounts began around the beginning of 2014, so our first tax return year was 2014. Because our revenue is under $50,000, we filed a 990-N e-postcard, acting as self-determined 501(c)(4). Of course, we will have to adapt filings as needed depending on our long-term [legal status](legal) which is not yet certain.
