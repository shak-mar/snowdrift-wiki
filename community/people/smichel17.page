---
title: User Profile — Stephen Michel
toc: false
...

**Once upon a time,** I decided to install
Linux on an old computer I had lying around, that could not run windows 7 (and,
having used 7, I didn’t want to go back to XP; linux also seemed cool). As I
was researching different distributions, I came across an article discussing
whether Ubuntu is an asset or liability to the Free Software movement.[^1] It
was fascinating; I was hooked.

**Since then** I’ve become a staunch advocate for FLO software. I use Fedora
Gnu/Linux as my primary operating system, and I am in the process of liberating
my Android phone.

**When I’m not working** on snowdrift.coop – currently as a
project manager, hopefully as a programmer once I learn Haskell – I’m probably
chatting about it in IRC, sleeping, playing games, or thinking about UX design.

**Website:** [smichel.me](http://smichel.me) [^2]  
**Updated on:** 2017-1-13.[^3]

[^1]: An asset because of the publicity they bring, a liability because of
their questionable commitment to the ideals of the movement.
[^2]: Hosted on [hcoop.net](https://hcoop.net), which is almost half as cool as snowdrift.coop.
[^3]: I believe in dating all static documents on the internet.
