---
title: Presentation Notes and Ideas
categories: communications
...

We want to develop ideas, narratives, key concepts, specific slide-shows, etc. for giving presentations about Snowdrift.coop. Obviously, we can utilize ideas from across the site (such as the [about stuff](https://snowdrift.coop/about), the [illustrations](/visual-design/illustration), and especially **[slogan](slogan)** which includes concise ideas for explanation of the system). Our [targeted-messaging](targeted-messaging) can help with different audiences.

We should develop a prioritized list of key points to adapt for presentations of different lengths.

## Resources

See [resources/presentations](/resources/presentations) for templates and files to use during public presentations.

## Some analogies and ideas for presentations:

### The problems with contrived discount sales

Consider contrived sales at retail businesses. Items are sold below cost to get people into stores where they can be enticed with other things. This is like providing no-charge resources with the catch that you have to watch ads. People who really take advantage of special retail sales but don't buy anything else may actually be hurting the business while those who aren't so careful are getting an unfair deal (i.e. are getting manipulated) in comparison. These specials have other annoying side-effects. What if I buy something and it goes on sale the next day? Etc. etc. Sales are gimmicky schemes that reinforce a consumer mentality divorced from actual connection with economic realities.

Business models based on artificial-scarcity are contrived in a similar way to gimmicky sales. 

The [time-limited fund-drive model of most crowdfunding](threshold-systems) works like short-term sales: high pressure, high-volume, highly marketed, gimmicky, and artificial. Snowdrift.coop has only a little artifice in our matching system, but this is directly connected to reality. In contrast to the artifice of failure or success defined in an artificial time window, we must truly all come together to some extent to have effective funding.

Snowdrift.coop is like *Trader Joe's* (a grocery chain that offers reasonable but variable prices reflecting reality of their costs and never ever has special sales) in that the experience is more honest without the obnoxious sales-discount gimmick.

Contrivances we want to avoid: artificial scarcity, planned obsolescence, sales and cut-off dates, random rewards for levels of contribution, pay-per-access or use for non-scarce goods, DRM, proprietary formats… All of these things hamper the natural flow of ideas and the market economy. Each also adds further disconnect between consumers and the process of production.

### Aaron's procrastination joke:

So, I was just studying music and got into ideas about tuning etc.

- what I really wanted was to just make the greatest music that I was imagining
- had to figure out the math and tools etc. first
- explored ethnomusicology / physics / psych
- decided to keep researching by attending grad school
- but was stressed out about requirements and organizing all the options
- so looked for organizing tools and found Task Coach but it wasn't perfect
- so volunteered to help with Task Coach development
- but needed more funding
- so started Snowdrift.coop

I started Snowdrift.coop to fund Task Coach so I could get organized so that I could apply to grad school so that I could get study more about music perception and tuning etc so that I could figure out how to best make some music ideas I had. I.e. This is just me being a typical creative artist being perfectionist or something and just procrastinating.

### About community solidarity / coordination

Adapt ideas from the [Comunes Manifesto](http://comunes.org/about/) regarding the **issue of disconnected nodes and lack of solidarity among grass-roots organizations and F/L/O commons producers**

Comunes manifesto is well, too weird in its wording about "elites", and so is just too divisive. But otherwise, the points are *great*, and they exactly represent our challenge with founding Snowdrift.coop. *Aaron:* I had been hesitant to do anything given this exact conundrum, but David pushed me anyway, and now we want to address this, and we're dealing with it in a different way from how others are attacking the problem.

### It is easier to avoid temptation than to resist it

*from Dan Ariely*

People often believe that they can do the right thing, that they will not be evil, dishonest, unfair, hurtful, etc. **Most people have good intentions.** Surely the majority of people at Google and Apple and Microsoft and others are mostly moral but imperfect people. **But conflicts of interest are very powerful.**^[See <https://www.ted.com/talks/dan_ariely_beware_conflicts_of_interest.html>]

Proprietary software and restricted permission culture and restricted science etc. all may be done by people who do not intend to do anything malicious and want to support everyone who they believe is honorable. We can give them the benefit of the doubt about their intentions. But these restrictions give them power. **Once you have power, you will be tempted to abuse it.**

It is true that we resist temptation often, and those in power could do worse things if they were truly evil people. But there is a limit to will power, and it is worn down by resistance to temptation. So those in power spend time resisting temptation to do corrupt things, exhaust themselves, and then weakly give in to temptation here and there and do pretty bad things. Once corruption sets in, it becomes a new norm and is easy to rationalize, and things devolve from there.

The solution is to *avoid* temptation. By making it very difficult or impossible to do certain malicious but tempting things, people keep the energy to resist the few temptations there still are (because we can never eliminate them all).

So, Snowdrift.coop is designed to maximize transparency, accountability, democratic control, and not give projects that much power over their products. These things serve to remove the temptations. When the project has little power to abuse and is held in check by the community, the developers are happy to be not tempted and to stick to being honorable.

## Extra presentation ideas

* On *freeloading*, consider *expense-loading!*

    From OfTheGaps on YouTube responding to a comment from Aaron about freeloading being the issue rather than "piracy" or "theft":

    >If we accuse consumers of free-loading, we must also accuse media giants of “expense-loading” (to coin a term). Digital media eliminates manufacture, shipping & storage expenses, yet media prices remain high. Why? Expense-loading. Consumers can’t expect something for nothing, but are expected to pay for media BEFORE knowing its value. Why? Expense-loading. The challenge is to develop a system eliminating﻿ *both* free-loading *and* expense-loading. If you keep one, expect the other.

## Discussing social dilemmas

In spring 2016, Aaron made a [series of short videos on social dilemmas](https://archive.org/details/AaronWolf-SocialDilemmas) as an example of how to talk about these topics generally..
