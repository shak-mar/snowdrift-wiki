<section class="wikiPageTools">
    <ul>
        <li><a href="$base$$pageUrl$?printable$if(revision)$&amp;revision=$revision$$endif$">Printable version</a></li>
        <li><a href="$base$/_showraw$pageUrl$$if(revision)$?revision=$revision$$endif$">Page source</a></li>
    </ul>
    <form action="$base$$pageUrl$" method="post" id="exportbox">
        <div class="select">
            <select name="format">
                <option value="LaTeX">LaTeX</option>
                <option value="ConTeXt">ConTeXt</option>
                <option value="Texinfo">Texinfo</option>
                <option value="reST">reST</option>
                <option value="Markdown">Markdown</option>
                <option value="CommonMark">CommonMark</option>
                <option value="Plain text">Plain text</option>
                <option value="MediaWiki">MediaWiki</option>
                <option value="Org-mode">Org-mode</option>
                <option value="ICML">ICML</option>
                <option value="Textile">Textile</option>
                <option value="AsciiDoc">AsciiDoc</option>
                <option value="Man page">Man page</option>
                <option value="DocBook">DocBook</option>
                <option value="DZSlides">DZSlides</option>
                <option value="Slidy">Slidy</option>
                <option value="S5">S5</option>
                <option value="EPUB">EPUB</option>
                <option value="ODT">ODT</option>
                <option value="DOCX">DOCX</option>
                <option value="RTF">RTF</option>
            </select>
        </div>
        <input name="export" id="export" value="Export" type="submit">
    </form>
</section>
