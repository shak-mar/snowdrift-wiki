# Meeting: Governance Meeting \#1

Time: 03/07/2016 07:00 PM - 10:00 PM (GMT+00:00) Casablanca

Location: meet.jit.si/snowdrift

Invitees: Aaron Wolf; Anita H.; Athan Spathas; Brian Smith; Bryan Richter; Charles Wyble; Iko ~; Jason Harrer; kenneth wyrick; Michael Siepmann; Peter Harpending; Robert Martinez; Stephen Michel; William Hale

Attendees: Aaron Wolf; Athan Spathas; Iko ~; Stephen Michel; William Hale

## Attendees (not listed above since not a member of OpenProject):

* Krasi

---

## Minutes

smichel17:
    - we should first designate a facilitator and secretary for meetings
    - HolacracyOne has posted their roles online: https://glassfrog.holacracy.org/circles/346
    - we can use their setup as inspiration, their group size is similar to ours and assign roles
    - role descriptions for facilitator and secretary:
        https://github.com/holacracyone/Holacracy-Constitution/blob/master/Appendix-A.md

Salt: are these fixed roles or do they change every meeting?
    
smichel17:
    - you can set up roles and have multiple people rotate taking on a role
    - there is a process whereby people can nominate and give feedback on nominations, but I think it's not really necessary, too formal
    
wolftune: we could have it as backup
    
smichel17: 
    - I can be the facilitator, given more familiarity with Holacracy processes
    - facilitator: takes ownership of conference room, can mute participants but not unmute
    - secretary:
        - sets up meetings and notify members of the meeting
        - capture output and publish, maintain
        - interpret Governance and institution at the conference
    - Governance meeting cheatsheet: http://www.holacracy.org/wp-content/uploads/2015/08/Governance-Meetings-card-bw.pdf

## Check-ins

* smichel17
** didn't get much sleep but excited about meeting applying Holacracy principles

* wolftune
** distracted and busy (kid on the way), happy to see other people stepping up and doing things
** we have a lot of loose ends and want to see things captured on OP with work packages

* Athan 
** I agree with wolftune, would like to see tasks really clear

* Salt
** I've been travelling and giving out over 70 business cards (at wikimedia, etc.)
** spent over 70 hours reading CiviCRM documentation, trying to map out CiviCRM organization on paper

* Iko
** a few loose ends the past week, closing a few tickets 
** waiting on user stories propagating to design direction/tasks, waiting on feedback from mray on a minor MR

smichel17:
    - Note: people are giving more detail than they really need to at this point, IMO. It's about getting focused on the meeting, not sharing progress updates.

## Notes

Salt: there was some discussion about running circles as sub-projects in OP, is this something we want to do?
Salt: I think we should have sub-circles, e.g. dev is a sub-circle
smichel17: I think we shouldn't have sub-circles, as you'd have to add secretary, rep links and add overhead
Salt: the default already lumps the responsibilities to on person so it wouldn't be much more overhead
smichel17: instead of talking about people, we should have separate roles 
wolftune: 
    - we don't have to decide on that now, I'm more interested in how we can use our current tools to track tasks and things
    - as Charles suggested for using redmine, sub-projects can be useful for different circles to track their tasks
Salt: I think we should place roles in top level of OP wiki
wolftune: we should focus on how to capture value when we have discussions e.g. Salt's suggestion about the wiki pages

smichel17:
    - let's go through and edit the accountabilities list in Etherpad
    - if you are doing something that's not already on this list, write it down
    - next, we go through the things and group them into roles

Salt: we should have a sysadmin team, as well as a group of people handling blog posts
wolftune: I'd like to see a blog post update on our new group structure
smichel17: I would posit it fits with the community role?
woltfune: so where should we put it? Salt, would it make sense to you to take on the promotion role?
Salt: sure

wolftune:  welcome, Krasi. We're working on setting up our organisational structure using Holacracy, relatively non-bureaucratic 
Krasi: I understand, empowering each participant to make decisions on their level
wolftune: we're trying to formalise this to make it more clear, and roles are independent of people
Krasi: it's good to have some overlap of responsibilities if possible. From a management perspective, if someone drops off, someone else can resume the tasks. For example, wolftune is going on leave soon, if we know what his responsibilities are, then the rest of the team can take up 1-2 roles each and continue to fill in until his return 
wolftune: smichel17 is our facilitator so in a minute we can do an intro and say hello
Krasi: my focus is on leadership and organisational change


