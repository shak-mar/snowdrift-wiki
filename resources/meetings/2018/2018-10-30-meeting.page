<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2018/ -->

# Meeting — October 30, 2018

Attendees: wolftune, smichel17, msiep

<!-- Check-in round -->

## Carry-overs from last meeting

```
## tracking ops tasks for just server maintenance
- NEXT STEP (chreekat): Review/capture ops priorities [chreekat, in his own list]

## Bryan/Stephen meeting — CI/CD improvements
- NEXT STEP (Bryan): write blog post about this process, document <https://git.snowdrift.coop/sd/snowdrift/issues/111>

## Governance overhaul / Volunteer recruitment
- NEXT STEP (Bryan): Decide whether we want to open up option of moving off of haskell for our web framework.
- NEXT STEP (wolftune): posts on discourse about things he has personal notes on governance roles.
- NEXT STEP (wolftune): co-working to create a more complete list of roles/titles we want (capture wolftune's notes in better form)

## Project outreach, Kodi etc
- NEXT STEP (Salt): Set up an hour to co-work with Michael
- NEXT STEP (Michael): work on feedback form <https://git.snowdrift.coop/sd/outreach/issues/41>

## CFPs
- NEXT STEP (Salt): Prepare CFP stuff, get feedback, check in at future meetings

## Private team contact info
- NEXT STEP (Salt): Spend an hour figuring out the right way to do this
- NEXT STEP (wolftune/smichel17): Remind Salt about this^ over the weekend

## Design progress plans
- NEXT STEP (msiep): Look through gitlab issues, do some grooming/organizing if you want.
```

## Metrics

Discourse (over past week):

- Signups: 2
- New Topics: 5
- Posts: 20

Sociocracy:

- Page 108

<!-- New Agenda Down Here  -->

## Real names vs aliases
- smichel17: I've been taking notes using aliases, considering real names instead. Opinions?
- wolftune: How we refer to people is a complicated thing, not sure there's a good answer. Maybe just ask people individually.
- smichel17: How would you personally like me to refer to you?
- wolftune, msiep: our nicknames are good, less ambiguity

## Meeting effectiveness & reliability of reaching people
- wolftune: I think meetings have been run well, mostly concerned with getting people here
- wolftune: mray, chreekat haven't been here for a while, but also there's other people who could join; we'd like to get more regular attendence
- smichel17: Being on chreekat's private server, no public address, is definitely a big barrier.
- wolftune: Maybe we could default to jitsi? It's not quite as reliable as mumble, but it's not bad and there's a lot of advantages.
- NEXT STEP: Try https://meet.jit.si/snowdrift next week. If it goes well, update discoure post and wiki.
- wolftune: Forum feels a little less reliable than other, more personal methods to reach people. Maybe there's no general solution, and we should just do it on a person-by-person basis (eg, I know Signal is good for reaching smichel17 and Salt, email for msiep, etc).
- smichel17: Could encourage team members to get emails for private messages, but that only works if people are good email people!
- smichel17: Maybe we could make a list of people & the best way to contact them? (relates to crm)
- wolftune: I chatted with Salt about that yesterday, he wants to stick with civi, just needs to get deployment status set so he can update misconfigured stuff
- smichel17: Does it make sense to make a (private) discourse page with that stuff for team members only until Salt gets a chance to do that?
- NEXT STEP (smichel17): Make a wiki post in the team section with people's contact info, special formatting for preferred & fast methods

## Discourse announce milestone
- wolftune: top priority is get blog post up, then we can reference it.
- wolftune: The big announcement at this point is to the point of "Hey, we're alive!", can also reference SeaGL
- wolftune: Everything except the announcing itself (discuss and announce lists, on social media) is done

## SeaGL etc
- wolftune: athan and I, and Salt, are going there, will probably be an FSF member meetup there.
- wolftune: thinking about how to promote to people, open to suggestions, it's in just under 2 weeks.
- wolftune: Also it's a milestone, any time you can put towards snowdrift for stuff you think would be worthwhile before SeaGL, please do
- smichel17: I'd say focus on recruitment at SeaGL and in announcements related to it, since we have a clear path but need people to shovel
- wolftune: May not be possible, but I'd like to get the stuff about titles we're recruiting for locked down before then.
- NEXT STEP (wolftune): Adjust priorities of tasks on gitlab

<!-- Decide where to capture outstanding tasks -->
<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->