<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2018/ -->
<!-- Meeting location: https://meet.jit.si/snowdrift -->

# Meeting — December 19, 2018

Attendees: wolftune, smichel17, mray, msiep

<!-- Check-in round -->

## Carry-overs from last meeting

```
## tracking ops tasks for just server maintenance
- NEXT STEP (chreekat): Review/capture ops priorities [chreekat, in his own list]

## Bryan/Stephen meeting — CI/CD improvements
- NEXT STEP (chreekat): write blog post about this process, document <https://git.snowdrift.coop/sd/snowdrift/issues/111>

## Tech volunteers and framework etc
- NEXT STEP (chreekat): Decide whether we're open to the option of moving off of haskell for our web framework

## Governance overhaul / Volunteer recruitment
- NEXT STEP (wolftune): posts on discourse about things he has personal notes on governance roles.
- NEXT STEP (wolftune): co-working to create a more complete list of roles/titles we want (capture wolftune's notes in better form)

## Project outreach, Kodi etc
- NEXT STEP (Salt): Set up an hour to co-work with Michael
- NEXT STEP (msiep): work on feedback form <https://git.snowdrift.coop/sd/outreach/issues/41>

## LFNW + beta milestone prioritizing/scheduling
- NEXT STEP (Salt, wolftune): Submit CFPs by January 31st <https://git.snowdrift.coop/sd/outreach/issues/50>
- NEXT STEP (smichel17): organize the above tasks on to gitlab in some way <https://git.snowdrift.coop/groups/sd/-/milestones/7>

## Board planning etc
- NEXT STEP: (wolftune) create organized milestone and tasks in governance repo for Board-related and other stuff…

## Knocking out code issues etc.
- NEXT STEP (smichel17): Look through issues, tag backend ones
- NEXT STEP (smichel17): announce alpha-visual-design milestone on forum (to other volunteer devs) <https://git.snowdrift.coop/groups/sd/-/milestones/6>
```

## Metrics

Discourse (over past week):

- Signups: 3 -> 1
- New Topics: 10 -> 7
- Posts: 37 -> 39

Sociocracy:

- Page 108

<!-- New Agenda Down Here  -->

## Governance
- wolftune: CANAWEN expressed interest in governance on the forums, 
- smichel: Overall seems 
- mray: What could he actually do?
- wolftune: I have a bunch of thoughts, need to study sociocracy more. Maybe he would read about sociocracy, have conversations with me or post on the forum, bring up discussions about how to improve our governance.
- wolftune: I suppose the first step would be to take the time to explain current status to him.
- NEXT STEP (wolftune): Follow up with canawen (explain current status, ask about his coding experience)

## Blog posts
- wolftune: blog post is published, you all see it?
- mray: Yes. Might have been better to split it up into smaller posts for more reasonable size and higher frequency
- wolftune: I think that makes sense, but I'm also concerned about unimportant posts burying important ones. Does anyone know about ghost's featured posts?
- NEXT STEP (wolftune): Investigate ghost's 'featured post' feature (try it, see what it looks like)
- mray: Are there ways to integrate discussion with mastodon? Ghost plugin? It would be nice, but would be more work…
- wolftune: we do already have comments via discourse
- mray: Discourse is more like an in-group; mastodon/activitypub is a different set of people
- wolftune: since we have had few posts, I want each post to be valuable and stand on its own.
- smichel17: If you start releasing more, smaller blog posts, that problem solves itself :P
- wolftune: Fair enough. And if I find myself writing a long one, I can always split it up and make a queue, to be released
- wolftune: does anyone actually propose we split the new one? (A: no, just for the future)

## Peertube video?
- mray: I uploaded it: <https://peertube.mastodon.host/videos/watch/53296741-b922-43d7-ac0f-a944d83af874>
- mray: I had some strange issues with playback on my machine, couldn't test it well. Haven't tested file size.
- wolftune, smichel17: I tried it and it worked
- mray: It could be good, but there's some risks (using people's bandwidth on mobile...). But it could be a good solution so we're prepared to handle a large volume of page hits
- {A bunch of discussion about peertube advantages & limitations}
- wolftune: Should I upload a compressed 1080p video?
- mray: I think we're good for now for testing.
- NEXT STEP (mray): Try to get the video up on peertube <https://git.snowdrift.coop/sd/snowdrift/issues/146>


<!-- Decide where to capture outstanding tasks -->
<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->