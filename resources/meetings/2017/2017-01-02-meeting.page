# Meeting — January 2, 2017

Attendees: iko. mray, MSiep, Salt, wolftune, smichel17

## Checkin

- iko: reduced availability starting this year, may still watch for
  illustration tasks
- started a branch with groundwork for alpha UI design. helped clean up issues
  section in new taiga projects and added project icons
- mray: busy this time of year but fine otherwise
- msiep: also distracted by family life, out of touch with email but will look
  at it soon
- salt: spending time getting contributions page stable to get civicrm
  donations processed, hopeful about 2017
- wolftune: getting settled into new routine, parents moved nearby recently and
  unpacking
- looking forward to hack through tasks and being able to focus on that
- doing okay while working on script and other things
- smichel17: out for a few days this new year and now back

---

## Agenda

- Meeting time (Salt) — still good given changed availabilities?
- Intro audio script (aaron)
- Following up with donors (aaron)
- Connecting with advisors (aaron)
- Strategies to keep us and other volunteers productive/on priorities (aaron)
- Alpha UI design branch update (iko)
- Intro to new taiga setup (smichel17)
- Discourse sso (Salt)
- Finding a time for a governance meeting -- next monday? (smichel17)
- My merge request in git (usability-improvements branch) (MSiep)


## Meeting time — still good given changed availabilities?

- Response from people present: meeting time is fine for now
- smichel17 will know schedule and availability soon


## Intro audio script

- Plan on after this meeting, if this goes too long, will discuss.


## Following up with donors

- Wolftune: Following up with donors — how soon will civi be working? Soon
  enough that we could wait until it's up to use it for this batch of donor
  rewards?
- Salt: Needs testing, transferring data from old one (to make sure we're not
  double-hitting people).
- Wolftune: Isn't the work in entering the data, so we could do that now?
- Salt: Emails are what matters. We want to make sure they're sent from the
  correct domain.
- Salt: Civi is designed to deal with an entire campaign. Everything is
  connected to Tilt campaign so we shouldn't add notes elsewhere.

**Next step:** use civi immediately, Salt hopes he will have it set up today.


## Connecting with advisors

- wolftune: there are a lot of people who would like to see us succeed. we
  should reconnect with them
- should we think about this now? trying to figure out how useful it will be,
  e.g. run questions by an advisor at this point in time
- smichel17: I'm not sure it would be useful to reach out to advisors compared
  to reaching out to volunteers who have contacted us in the past.
- it seems we've lost some people to do tasks, so additional volunteers would
  be helpful
- Salt, MSiep, iko: I agree
- wolftune: as far as reaching out to volunteers, civi is still the main focus,
  I'll try to be patient and wait for it to work out


## Strategies to keep us and other volunteers productive and on priorities

- wolftune: it's useful when people check in with other people. "Hey, did you
  get to [that]?" Ends up increasing productivity overall.
- Salt: I'd like to have a shared calendar somewhere, so people can see my
  availability (e.g. this time working on snowdrift and will be on Mumble)
- smichel17: +1
- Salt: irc is helpful but it's not the same as working together
- smichel17: I'm not sure about the particular implementation, but the time I
  spend working with someone have been the most productive
- wolftune: how about scheduling co-working times? a running list to keep
  things up every day, who has availability daily or weekly basis
- smichel17: I have a calendar instance on nextcloud. I can give you accounts
  on the server, you share your calendar with others in the group
- salt: there would be sysadmin overhead involved. smichel17, will you be okay
  with running or sharing that instance?
- MSiep: my problem would be to manually keep calendars in sync, but one
  snowdrift calendar with e.g. meeting dates would be helpful
- wolftune: the challenge is there's a lot of human overhead and error
- MSiep: seems complicated to me, my calendar is running on owncloud running on
  a basement home server (it doesn't connect to the internet)
- salt: I like having separate calendars and being able to turn on/off a
  calendar for snowdrift. I can block out some time for snowdrift on it
- wolftune: it may be better to ask people on irc their availability for a
  particular task in the next hour
- Salt: my issue with it is having to stop whatever I'm doing. I like to
  schedule time instead
- wolftune: how do we get a shared calendar? smichel17, would you like to look
  into setting up a shared calendar?
- smichel17: sure

**Next step:** smichel17 will investigate shared calendar setup in nextcloud.


## Alpha UI design branch update

- iko: I spoke with JazzyEagle a few days ago about the new site design
  implementation. the plan was to start by importing the new font and global
  stylesheets, then implement page by page
- I made a new alpha-ui-design branch with this, and a bit more
- I'm done with the branch and it's now passed on to JazzyEagle (I haven't had
  a chance to speak with him yet since starting the branch, but left him a
  message with taiga link on irc)
- status is currently posted on Taiga wiki (not sure what JazzyEagle wanted to
  use, only a few parts were already being tracked on taiga, it was also
  quicker to edit a text file)
- <https://tree.taiga.io/project/snowdrift-dev/wiki/alpha-ui-design>
- the branch incorporates most of MSiep's text changes from his
  usability-improvements branch. changes not added: css container resize on
  auth/login (cassius no longer exists and there is a new layout for auth
  pages) and login fields labels (2 instances of unconfirmed backend changes,
  including changing tests and duplicating the auth form respectively)
- wolftune: I can start a new branch in the dev repo to accept MRs for this
  branch
- iko: that would be great, ty. wasn't sure of the procedure. if there are
  other questions, I'd be happy to answer
- mray: are we at a point where we have a defined workflow for working on the
  design in the dev repo?
- iko: almost. when wolftune sets up the new branch, people can download the
  branch, run it and make changes from there
- we had an issue getting sass support in Yesod with Haskell libraries. the
  temporary workaround is to use a ruby sass compiler to generate the
  stylesheets until it's resolved upstream (instructions on the taiga wiki)
- as a next step, maybe wolftune, mray and msiep, along with others interested,
  would like to arrange a time to discuss the feedback?
- then a meeting with JazzyEagle/dev to clarify what (if any) changes should be
  made
- mray: do we know JazzyEagle's availability?
- iko: I can ask. he's usually around during the weekdays around 17:00 UTC and
  earlier, away during weekends

**Next steps:**

- wolftune will get a working branch for alpha-ui-design in dev repo
- wolftune has feedback for prototype, to meet with design leads to discuss
  (after working branch set up)
- iko will check with JazzyEagle on meeting availability (design to discuss
  changes as outcome of feedback review above and work with dev to resolve
  outstanding issues)


## Intro to new taiga setup

- smichel17: with iko's help, I've split up the snowdrift project into 3 other
  projects, one for each Circle. this will allow for more flexibility within
  projects, e.g. if development wants to modify their kanban to suit their
  workflow
    - https://tree.taiga.io/project/snowdrift-dev
    - https://tree.taiga.io/project/snowdrift-ops
    - https://tree.taiga.io/project/snowdrift-outreach
- introducing Epics https://tree.taiga.io/project/snowdrift/epics this page can
  replace the /planning/next snowdrift wiki page so there's no need to manually
  update a page, just look at the milestone epic for list of related User
  Stories and Tasks
- wolftune: can I access a landing page that explains all of this?
- Salt: I propose epics and the new changes be updated on the taiga wiki page.
  thanks for all the work setting this up!
- smichel17: I can do that

**Next step:** make a 'home page' for planning, with links to the different
projects. (smichel17)


## Discourse SSO

- Salt: (an update, not discussion) I wanted to mention that there's an email
  thread that hasn't gathered discussion about discourse sso


## Finding a time for a governance meeting

- smichel17: Salt and I made some changes to the governance docs that we'd like
  to propose. can we set up a meeting for this?
- smichel17: how about next monday, same time as the meeting, work for people?
- Salt: I'd like to give at least a 1-week notice so more people can show
  up/give feedback

**Next step:** send out email asking about availability, particularly for a
governance meeting. (smichel17)


## MSiep's merge request in git (usability improvements branch)

- Salt: MSiep, I know we touched upon this topic earlier in the alpha-ui-design
  update, is there anything else you wish to discuss about the MR?
- MSiep: I wanted to ask what needs to happen for it to go through. I'd like to
  check if it makes sense in the future (I'm still learning to work with git) f
  I have simple text changes and backend code changes, to separate them out
  into different branches, so the first can be merged without requiring
  chreekat's attention?
- wolftune: if they're truly standalone, then yes. however, in this case it was
  more a matter of circumstances (chreekat's changed schedule). you've done
  what's needed and it's waiting for chreekat's response
