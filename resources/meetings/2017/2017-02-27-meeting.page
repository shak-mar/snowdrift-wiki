# Meeting — February 27, 2017

Attendees: wolftune, Salt, MSiep, mray, ikomi, cgag

---

## Agenda

- Brief update on animation video (salt)
- Stuff for Curtis to do (Haskell mainly, clear up issues, etc.) (wolftune)
- Status of conference planning (wolftune)
- Goals for CiviCRM+Discourse (Salt)
- mx1 Dockerization Update (Salt)

## Brief update on animation video

- salt: would you like to give a brief update on the intro video?
- mray: I've been working with Johannes over seafile
- some elements need redo visually or textually, we haven't figured it out yet,
  still working on it


## Stuff for Curtis to do

- cgag: I looked at some issues and it's not clear where they stand, they're
  open but the prototype looks good
- where is the code for stephen (I believe this is salt?)'s prototype
- salt: welcome, curtis! let's look at what you can help with
- wolftune: for financial reasons, our dev chreekat had to leave to get another
  job position, but he isn't gone
- we want to make sure he stays on top of things that get added to the codebase
- I'll give you a summary of the current status of things
- cgag: that would be great, I'm not sure where everything stands at the moment
- wolftune: the codebase now has been re-organised and some of the
  crowdmatching features in place
- the wiki and tickets from before have been separated out, we now use gitit
  and discourse
- salt has set up discourse for forum discussions. jazzyeagle worked on
  discourse single sign-on (sso)
- how do you feel about writting appropriate tests for haskell?
- cgag: that seems like a good starting point
- wolftune: I think jazzyeagle has got the sso covered feature-wise
- he's a novice and not sure how to write tests without dependency for others
  to install and run a discourse instance
- it's the last technical thing towards integrating discourse
- discourse will open up the ability to work on things looser than formal
  tickets
- cgag: yeah, I'll get into that when I can
- wolftune: I'll find links so you can see which repos to look at, etc.
- mray: not a dev, but anything that enables sass to be used in the codebase
  would be a good advancement for design
- wolftune: that's another item you can take a look at, cgag
- we'd like to get the code working with ghc 8, and then be able to look at
  suitable sass packages
- I think chreekat wants to do code review or something and was hoping someone
  will look into it
- sass has a number of advantages, so we'd like to let the designers work with
  sass and let it compile
- cgag: was sass being used before?
- wolftune: no, we were using cassius at one point. there's currently an
  alpha-ui-design branch in the repo
- the sass is currently generated with a compiler (not haskell) and someone has
  to manually run the compiler to see the results
- iko, is this documented anywhere?
- iko: <https://tree.taiga.io/project/snowdrift-dev/wiki/ruby-sass>


## Status of conference planning

- salt: I've collected up some materials and still need to update the wiki
  pages:
- <https://wiki.snowdrift.coop/communications/publicity#conferences>
- <https://wiki.snowdrift.coop/community/events>
- salt: there's LFNW coming up and LibrePlanet
- wolftune: let's focus on LibrePlanet
- salt: discourse and civicrm will be launched by LibrePlanet, they have to be
  up by then as they're related to my talk or I'd be in trouble :)
- maybe we could get some posters targeted for the event so we can do some
  marketing?
- mray: what are you talking about in terms of size and content?
- salt: something like the one for FOSDEM. FOSDEM is a dev-centric conference,
  LibrePlanet is a bit more free software and advocacy event
- LFNW is a more community-focused event. OSCON I won't be able to make it but
  may get someone out there
- it's a more business-oriented event, but there are also some open source
  people there


## mx1 Dockerization Update

- salt: I haven't had a chance to incorporate the docker scripts iko made
- civicrm and discourse have bumped to the top priority because of LibrePlanet
- cgag: I deal with containers a lot, so if you'd like help, you can run it by
  me
- salt: thanks. just putting it out there, have you tried discourse's docker
  manager?
- cgag: I haven't played with it at all
- salt: I'll speak with you later about it


## Goals for CiviCRM+Discourse

- salt: my talk isn't about snowdrift specifically, but the goals leading up to
  snowdrift and the use of civicrm
- let me know if you have any suggestions on points to touch on in the talk
- wolftune: I would suggest focusing on the human element (i.e. there's this
  project called snowdrift.coop, which uses civicrm for various purposes)
- salt: important features include tracking the donations, send announcements,
  volunteer form
- any thoughts on the connection to discourse? anyone has ideas how that would
  look like?
- wolftune: tagging is a big thing. e.g. skillset or background people have,
  people connected with organisations
- the people you're talking to and know how to bring relevant things to the
  people

